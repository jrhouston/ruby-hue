#!/usr/bin/env ruby

require 'rubygems'
require 'aws-sdk'
require 'optparse'
require 'json'
require 'logger'
require 'getopt/long'

# Class to add messages to an SQS Queue
class HueQueue

  attr_accessor :hue,:logger

  # Initialize the HueQueue class
  def initialize(options,level = nil)

    @logger = Logger.new('./logs/queue.log')  # Set log file
    @logger.level = Logger::INFO if level == nil  # Set logging level
    @logger.level = level if level != nil

    begin
      @options=options # Get options

      @creds = Aws::Credentials.new(@options[:access],@options[:secret])
      @sqs = Aws::SQS::Client.new(region: @options[:region],credentials: @creds)
                                   #:sqs_endpoint => "sqs.#{@options[:region]}.amazonaws.com")
    rescue Exception => e
      @logger.error("Invalid Client Token ID: %s " % e) # If there is an issue...
    end
  end

  # Method to send payload to SQS
  def send action, location

    # Payload conists of a command action and location
    # This allows specific commands to be sent to specific locations
    # Example location: Edinburgh action: red will tell the lights in Edinburgh to change to red
    location.each do |lo | 
      

    
    payload = JSON({"hueCommand"=>{
                      :location => lo,
                      :action => action.to_s,
                      :sent => Time.new().to_s
                    }
                    })

    logger.info("Sending: %s" % payload.to_s)
    begin
      # Create send Hash with queue details and payload added
      send = {
        queue_url: @options[:queue],   # Queue URL provided at initalization
      message_body: payload}           # message_body set to payload created previously

      @sqs.send_message(send)

      logger.info("Sent: %s" % payload.to_s)
    rescue Aws::SQS::Errors::InvalidClientTokenId => e
      @logger.error("Can Not send %s - Invalide Client Token ID: %s " % [e,payload.to_s]) #If there is an issue...
    end
    end
  end
end

# Load config_file
config_file = File.read("./config/poll.json")
config = JSON.parse(config_file,{:symbolize_names => true}).to_h # Parse the config file from JSON into a Ruby Hash

parameters = Getopt::Long.getopts(
  ["--location","-l",Getopt::REQUIRED],
  ["--action","-a",Getopt::REQUIRED]
)


options = {:action => parameters["a"],
           :location => parameters["l"].split(',')}

queue = HueQueue.new(config,Logger::INFO)
queue.send(options[:action],options[:location])
