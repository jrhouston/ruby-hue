require 'hue'
require 'json'
s ={
			"red" => {
		:brightness =>254,
		:saturation =>243,
		:hue => 65257,
		:on => true,
		:transition => 1
	},
	"green" => {
		:brightness =>254,
		:saturation =>254,
		:hue => 25699,
		:on => true,
		:transition => 1
	},
	"normal" => {
		:brightness =>254,
		:saturation =>144,
		:hue => 14922,
		:on => true,
		:transition => 20	
	},
	"blue" => {
		:brightness =>255,
		:saturation =>255,
		:hue => 46920,
		:on => true,
		:transition => 10	
	},
	"blue_dim" => {
		:brightness =>103,
		:saturation =>255,
		:hue => 46920,
		:on => true,
		:transition => 10	
	},
	"off" => {
		:brightness =>0,
		:saturation =>0,
		:hue => 0,
		:on => false,
		:transition => 10	
	},
	"pink" => {
		:brightness =>255,
		:saturation =>255,
		:hue => 56366,
		:on => true,
		:transition => 10	
	}
}

puts JSON(s)