require './huepoll'

config_file = File.read("../config/poll.json")   # Load the config
config = JSON.parse(config_file,{:symbolize_names => true}).to_h # Convert config JSON to a Hash

options = {}

# Parser Setup
parser = OptionParser.new do | opts |
  opts.banner = "Usage: SnapshotRemoval.rb [options]"

  # Option for AWS IAM accesss Key
  opts.on("-l", "--access_key access_key", "location") do |l|
    config[:location] = l
  end


end.parse!

# Create new HuePoll object, passing in the config and if you require debugging (default is false)
poll = HuePoll::Client.new(config)

loop=true

while loop==true do
    poll.query
  end
