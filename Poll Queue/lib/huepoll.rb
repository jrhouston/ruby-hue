require 'rubygems'
require 'aws-sdk-v1'
require 'json'
require 'optparse'
require 'date'
require 'time'
require './huepoll/Client'
require './huepoll/CRHue'
require 'logger'


module HuePoll
  version = "0.1"
end
