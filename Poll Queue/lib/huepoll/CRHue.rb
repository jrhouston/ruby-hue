require 'hue'
require 'json'
require 'logger'

module HuePoll

  class CRHue

    attr_accessor :client, :scenes, :group, :user,:logger

    def initialize u
      @user = u
      load_light_config
      @logger = Logger.new('../logs/hue.log')
    end


    def load_light_config

      config_file = File.read("../config/hue_config.json")
      config = JSON.parse(config_file).to_h
      @scenes = config["scenes"].to_a

    end

    def getLights

      lights = []
      if @client != nil
        @client.lights.each do | l |
          lights << l if l.reachable?
        end

        return lights
      else

        return 0

      end
    end

    def connect

      begin
        @client = Hue::Client.new(@user)
        return "connected"
      rescue Hue::LinkButtonNotPressed => e
        return e
      rescue Hue::NoBridgeFound => e

        @logger.error("No Hue Bridge found!")
        @client = nil
        return "No Hue Bridge found"

      end
    end

    def connected?

      if @client.li.count > 0 then
        return true
      else
        return false
      end
    end

    def detail_lights lights

      lights.each do | l |

        puts "Name: %s ID: %s Colormode: %s Brightness: %s Hue: %s Saturation: %s" % [l.name,l.id.to_s,l.color_mode,l.brightness,l.hue,l.saturation]
      end
    end

    def all_on

      @client.lights.each do | l |
        l.set_state({:on => true, :saturation => 243, :brightness => 254,:hue => 65257}, 1)
      end
    end

    def setLights lights,settings

      lights.each do | l |

        l.set_state({:on => settings["on"], :saturation => settings["saturation"], :brightness => settings["brightness"],:hue => settings["hue"]},settings["transition"])
        @logger.info("%s has been changed to %s ",l.name,settings)
      end

    end

    def setGroup groupname, settings

      puts settings

      group = @client.bridge.groups.select { | g | g.name == groupname}[0]
      puts group.lights

      puts settings[:on]

      group.set_state({:on => settings["on"], :saturation => settings["saturation"], :brightness => settings["brightness"],:hue => settings["hue"]})
      @logger.info("Group %s state has been changed to %s " % [groupname,settings.to_s])

    end

    def multiStates lights, steps

      steps.each do | step |

        setGroup "Office Lights", step[:scene]
        sleep step[:interval]
      end

    end

  end
end
