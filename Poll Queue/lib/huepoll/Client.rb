
module HuePoll
  class Client

    attr_accessor :hue, :commands, :mins_remaining, :debug,:log

    # Set up HuePoll
    def initialize(options,debug = false)
      begin
      @options=options

      @logger = Logger.new('../logs/poll.log') # Set up logging location
      @debug = debug

      # Setup poller object - an AWS SQS client
      @poller = AWS::SQS.new(
        :access_key_id => "#{@options[:access]}",
        :secret_access_key => "#{@options[:secret]}",
      :sqs_endpoint => "sqs.#{@options[:region]}.amazonaws.com")

      @hue = CRHue.new("rubyCat2015") # Setup CRHue Object
      @hue.connect

      load_light_config               # Load configuration of possible actions

      puts "Waiting for Commands"
      @logger.info("Waiting for commands")
rescue Hue::NoBridgeFound => e
  puts "e"

end

    end

    def load_light_config

      config_file = File.read("../config/config.json") # JSON File with possible actions that are supported by CRHue
      config = JSON.parse(config_file).to_h           # Create Ruby hash from JSON
      @commands = config["commands"].to_a             # Get array of commands

    end

    def retrieve

      # Get a SQS message from specific queue
      response = @poller.client.receive_message(
        :max_number_of_messages => @options[:maximum],
      :queue_url => "#{@options[:queue]}")

      response.messages # return messages obtained
    end

    def query

      # Query method that will query the SQS queue for new commands

      loop=true # Set up loop to continusly check the queue

      while loop==true
        responses = retrieve # Get any messages

        if responses.empty?
          loop=false  # If no responses, close the loop
        else
          skip = false
          
          
          # For each message recieved
          responses.each do |msg|
            receipt = "#{msg[:receipt_handle]}" # Get the message receipt

            commands = JSON(msg[:body]).to_h["hueCommand"] # Get command from message
            location = commands["location"]
            
                if location == @options[:location] then # Check if the location of the message matches the location being polled for
                  @logger.info("Command recieved")
                  puts "Command recieved"
                    timenow = Time.new()
                    timesent = Time.parse(commands["sent"].to_s)
                      puts timesent.to_s
                      puts timenow.to_s
                      sentAgo = (timenow - timesent) / 60

                      puts sentAgo
                
                    if sentAgo < 5 then
                      puts "Less than 5 mins"
                
                      
              
                        # For each of the locations found in the command
                        # If message location matches polling location

                        puts "Command matches location"
                        @logger.info("Command matches location")
                        lights = @hue.getLights # Get CRHue lights if not in debug mode

                        if lights != 0 then
                          setLights commands["action"],lights # Attempt to set the lights to the specified command
                        else
                          puts "No lights connected!"
                        end
                      puts "Completed %s" % commands["action"]
                      @logger.info("Completed %s" % commands["action"])
                    else
                      @poller.client.delete_message(
                        :receipt_handle => receipt,
                        :queue_url => @options[:queue])
                      next
                    end
                  else
                    next
                  end
            # Delete the message from the queue once processed. Uses the message recieipt and the queue endpoint
            @poller.client.delete_message(
              :receipt_handle => receipt,
            :queue_url => @options[:queue])

          end
        end
      end
    end


    # Method to check when the specified command was last run
    # This is to stop the same command being run too many times.
    # @param cmd = command to check
    def check_last_run cmd

      now = Time.now # Get current time

      # Check if the commands last used time is valid (Nil means not run and if min_interval is > than the time of last used, then able to run)
      if cmd[:command]["last_used"] == nil ||  (now - (cmd[:command]["min_interval"] * 60)) >= Time.parse(cmd[:command]["last_used"]) then
        @commands[cmd[:pos]]["last_used"] = now.to_s  # Set the commands last_used time to current time
        return true # Return true if command is able to be run
      else
        # If action can not be used
        now += (cmd[:command]["min_interval"] * 60) # Add min_interval to the current time object
        @logger.warn(now.strftime("You need to wait until %H:%M to run this command again"))
        return false
      end

    end


    # Method responsible for communicating changes to the CRHue object
    # @param a = action to run (Hash)
    # @param lights = (Array of Hue::Lights)
    def setLights a,lights

      cmd = {:command => nil, :pos => nil} #Create base command hash

      #For each of the commands (including their index in the array)
      @commands.each_with_index do | c , i |
        if c["name"] == a then #If the name matches the action
          cmd[:command] = c    # Set the command to the current command
          cmd[:pos] = i        # Set the position to the current position
          break
        end
      end

      # If the position isn'r nil (a match was found)
      if cmd[:pos] != nil then

        # Check if the command is able to be run (based on commands minimum interval)
        if check_last_run cmd then

          steps = cmd[:command]["steps"] #Get the steps array of the command

          if steps.count == 1 then #If there is only 1 step
            puts 'only 1'
            step = @hue.scenes.select{|s|s["name"] == steps[0]["step"]} #Get that step
            @hue.setGroup "Office Lights",step[0] if step.count == 1  # Execute the step
            puts 'done 1'
          else # If there is multiple steps
            s = []  # Create empty array to store each step in

            # Loop through the commands steps
            steps.each do | st |
              step = @hue.scenes.select{|s|s["name"] == st["step"]} # Get the current step
              s << {:scene => step[0],:interval =>st["interval"]} if step.count > 0 # Add the step to the array of steps
            end

            @hue.multiStates lights, s # Execute the steps
          end

        else
          # Command was requested before minimum interval had passed
          @logger.info("Command sent too soon %s"%a)

        end

      else
        # Command was not found
        @logger.info("Command %s not found" % a)
      end
    end
  end
end
  